# git-repo4each

## _Hackable, scriptable repo manager in git + BASH_

git-repo4each updates a list of GIT repositories; you can commit, push, pull, or execute a custom script in each worktree (or for each branch) of each repository.

## Copyright

`Alin Mr. <almr.oss@outlook.com>` / MIT license

## Installation

Put `git-repo4each` in `PATH` (e.g. in `/usr/local/bin`) and call it via `git-repo4each` or `git repo4each`.

## Example: multi-repo log

`cd` to a directory containing `git` repos, then
```
cmd='p=$(basename "$GITREPO4EACH_COMMON_TOP")
  git log --pretty="%ad %s [$p::$GITREPO4EACH_BR]" --date=short'
git-repo4each -F -s <(ls -d */) -v --bash "$p" 2>/dev/null |
  LC_ALL=C sort | less
```
This will print a combined log of **all commits in all repos, all branches** (and could be written as a one-liner!). [`examples/git-logall-tsv`](./examples/git-logall-tsv) generates a tab-separated log suitable for importing into a spreadsheet.

## Usage

```
Usage (see README.md for details): git-repo4each [OPTION]...
-h | --help
-v | --verbose
-b | --branches | --no-need-worktree
-c | --cfg | --source-bash-script
-f | --force
-F | --no-fetch
-l | --local | --no-need-upstream
-s | --sources | --repo-list-file
-y | --no-confirm
--clean-tmp-on-exit
--setup-shell
--use-internal
--exec
--bash
--pull
--push
--status

E.g.
$ git-repo4each # use $PWD
$ git-repo4each -s repo-list.txt --bash 'git log'
$ git-repo4each -s <(echo '/path/to/repo')

With no configured sources, defaults to current directory.
```

Any script or executable run wih `--exec` has access to global, per-repo, and per-branch exports (search for `#.*exports` in the source). Additionally, scripts run with `--use-internal` have access to a couple of functions (search for `setup_internal_api`).

You can see all `git-repo4each` exports / functions, uncategorized, using the command below; you'll have to search the source as described to see up-to-date information.
```
git-repo4each --no-fetch --clean-tmp-on-exit --use-internal --bash 'env |
 grep GITREPO4EACH | sort; declare -F |
 sed -En "s/^declare -f (__g4er_)/\\1/p"; __g4er_abort' 2>/dev/null

# Current names (values are just examples)
GITREPO4EACH_BR=master
GITREPO4EACH_COMMON_TOP=/usr/local/src/git-repo4each
GITREPO4EACH_LOGFILE=/tmp/git-repo4each.ljOd/update.log
GITREPO4EACH_O_CONFIRM=true
GITREPO4EACH_O_LOCAL=false
GITREPO4EACH_O_NEEDWT=true
GITREPO4EACH_PUSH=origin/master
GITREPO4EACH_PUSHREMOTE=origin
GITREPO4EACH_REMOTE=origin
GITREPO4EACH_TMP=/tmp/git-repo4each.E0jN
GITREPO4EACH_TRACK=
GITREPO4EACH_TRACKSHORT==
GITREPO4EACH_UPSTREAM=origin/master
GITREPO4EACH_WT=/usr/local/src/git-repo4each
__g4er_abort
__g4er_keepon
__g4er_l
__g4er_reload_exports
__g4er_slurp
```

Brief summary (non-obvious stuff):
- `GITREPO4EACH_TMP` is the per-job control / temporary directory
- `GITREPO4EACH_O_*` are global options affecting behavior (e.g. whether to ignore remote-less or worktree-less branches)
- `GITREPO4EACH_COMMON_TOP` is the top-level directory for the main worktree, or `/path/to/bare-repo.git`
- `GITREPO4EACH_WT` is the worktree path (if any)
- `__g4er_l()` logs (e.g. `-el $msg` outputs to `stderr` and logfile; `$msg = -` means read piped-in input)
- `__g4er_abort()` asks the entire job to terminate ASAP
- `__g4er_keepon()` has non-zero exit status if termination has been requested

## Other software

- [gita](https://github.com/nosarthur/gita)
- `git-bulk` from [git-extras](https://github.com/tj/git-extras)
- https://github.com/isacikgoz/gitbatch
- https://github.com/earwig/git-repo-updater

Compared to these, `git-repo4each` focuses on non-interactive usage, multiple worktree support and scriptability (hence the internal API and the per-repo / branch exports).
